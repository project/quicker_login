## Quicker Login Module

The Quicker Login module is a developer module that once enabled
allows you to view any page as any user or login as any user.


### Requirements

No special requirements.

### Install

Install as you would normally install a contributed Drupal module.

See:
https://www.drupal.org/documentation/install/modules-themes/modules-8

### Usage

To log in as any user, visit: `/user/ql/{user_name}`

To visit a page as any user e.g. the content listing,
visit: `/admin/content?ql={user_name}`

Note: While the module is enabled a warning message will be
displayed on the site.

### Maintainers

Current Maintainers:
* George Anderson - https://www.drupal.org/u/geoanders

Past Maintainers:
* Michael Welford - https://www.drupal.org/u/mikejw

This project has been sponsored by:
* The University of Adelaide - https://www.drupal.org/university-of-adelaide
  Visit: http://www.adelaide.edu.au
