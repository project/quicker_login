<?php

namespace Drupal\quicker_login\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * QuickerLogin service.
 */
class QuickerLoginService implements QuickerLoginServiceInterface {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Session manager.
   *
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  protected $sessionManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountProxyInterface $current_user, ModuleHandlerInterface $module_handler, SessionManagerInterface $session_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->moduleHandler = $module_handler;
    $this->sessionManager = $session_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function loginUserName($user_name) {
    // Get target user.
    $users = $this->entityTypeManager->getStorage('user')
      ->loadByProperties(['name' => $user_name]);
    if (!$users) {
      return FALSE;
    }
    $target_user = reset($users);

    // If not anonymous user.
    if (!$this->currentUser->isAnonymous()) {
      // Log out the current user.
      $this->messenger()->addMessage($this->t('Already logged in. Logging out user and destroying session.'), 'warning');
      $this->moduleHandler->invokeAll('user_logout', [$target_user]);
      $this->sessionManager->destroy();

      // Send back to Quick login.
      $response = new RedirectResponse('/user/ql/' . $user_name);
      $response->send();
      return FALSE;
    }

    // Finalize user.
    user_login_finalize($target_user);
    return TRUE;
  }

}
